CC = gcc
CXX = g++
CFLAGS = -Wall -g -O2 -O3 -DENABLE_THREADS -D_REENTRANT
LIBS = -L../src/ -lircclient -lpthread 
INCLUDES=-I../include

EXAMPLES=main

all:	$(EXAMPLES)

main:	main.o
	$(CC) -o main main.o $(LIBS)

clean:
	-rm -f $(EXAMPLES) *.o *.exe

distclean: clean
	-rm -f Makefile *.log


.c.o:
	@echo "Compiling  $<"
	@$(CC) $(CFLAGS) $(INCLUDES) -c -o $@ $<

.cpp.o:
	@echo "Compiling  $<"
	@$(CXX) $(CFLAGS) $(INCLUDES) -c -o $@ $<
