/*
 * Haupt Datei für den IRC Bot
 * Achtung: Wichtig bei der Parameter übergabe, der Server muss innerhalb eines '' geschrieben werden
 * Bsp.: '#ircbottest'
 * Wichtig: Innerhalb der SRH funktionert es nicht wegen der Port Beschränkung. Port freie Internet Benötigt!
 */

#include "include/IRC_Funktionen.h"


int main (int argc, char **argv)
{
	irc_callbacks_t	callbacks;
	irc_ctx_t ctx;
	irc_session_t * s;
	unsigned short port = 6667;

	//  Überprüfen ob 4 Parameter übergeben wurden
	if ( argc != 4 )
	{
		printf ("Usage: %s <server> <spitzname> '#chatraum'\n", argv[0]);
		return 1;
	}

	// erstellung der callbacks (das sind die events auf die der bot reagiert bzw. die funktion aussruft)
	memset (&callbacks, 0, sizeof(callbacks));

	callbacks.event_connect = event_connect;
	callbacks.event_join = event_join;
	callbacks.event_nick = dump_event;
	callbacks.event_quit = dump_event;
	callbacks.event_part = dump_event;
	callbacks.event_mode = dump_event;
	callbacks.event_topic = dump_event;
	callbacks.event_kick = dump_event;
	callbacks.event_channel = event_channel;
	callbacks.event_privmsg = event_privmsg;
	callbacks.event_notice = dump_event;
	callbacks.event_invite = dump_event;
	callbacks.event_umode = dump_event;
	callbacks.event_ctcp_rep = dump_event;
	callbacks.event_ctcp_action = dump_event;
	callbacks.event_unknown = dump_event;
	callbacks.event_numeric = event_numeric;

	callbacks.event_dcc_chat_req = irc_event_dcc_chat;
	callbacks.event_dcc_send_req = irc_event_dcc_send;


	// Callbacks der Irc Session übergeben
	s = irc_create_session (&callbacks);

	if ( !s )
	{
		printf ("Konnte IRC Session nicht erstellen\n");
		return 1;
	}

	ctx.channel = argv[3];
    ctx.nick = argv[2];

	irc_set_ctx (s, &ctx);

	if ( strchr( argv[1], ':' ) != 0 )
		port = 0;

	// Wenn eine Verbindung über SSL nicht möglich aufgrund der Zertifizierung
	// dann wird eine Verbindung ohne Verifizierung versucht
	if ( argv[1][0] == '#' && argv[1][1] == '#' )
	{
		argv[1]++;
		
		irc_option_set( s, LIBIRC_OPTION_SSL_NO_VERIFY );
	}
	
	// Initiate the IRC server connection
	if ( irc_connect (s, argv[1], port, 0, argv[2], 0, 0) )
	{
		printf ("Konnte nicht Verbinden: %s\n", irc_strerror (irc_errno(s)));
		return 1;
	}

	// and run into forever loop, generating events
	if ( irc_run (s) )
	{
		printf ("Verbindung unterbrochen oder I/O fehler: %s\n", irc_strerror (irc_errno(s)));
		return 1;
	}

	return 1;
}
