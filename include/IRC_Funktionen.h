/*
 * Alle Funnktionen die für den Bot benötigt werden
 * Als Grundlage dient die Libircclient Bibliothek
 */

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>

// Einfügen der Libirclient Bibliothek
#include "libircclient.h"


/*
 * Wir speichern die Strucktur der IRC Verbindung
 */
typedef struct
{
	char 	* channel;
	char 	* nick;

} irc_ctx_t;

// Erstellung und Erweiterung einer Log Datei
void addlog (const char * fmt, ...)
{
	FILE * fp;
	char buf[1024];
	va_list va_alist;

	va_start (va_alist, fmt);
#if defined (_WIN32)
	_vsnprintf (buf, sizeof(buf), fmt, va_alist);
#else
	vsnprintf (buf, sizeof(buf), fmt, va_alist);
#endif
	va_end (va_alist);

	printf ("%s\n", buf);

	if ( (fp = fopen ("ircchat.log", "ab")) != 0 )
	{
		fprintf (fp, "%s\n", buf);
		fclose (fp);
	}
}


void dump_event (irc_session_t * session, const char * event, const char * origin, const char ** params, unsigned int count)
{
	char buf[512];
	int cnt;

	buf[0] = '\0';

	for ( cnt = 0; cnt < count; cnt++ )
	{
		if ( cnt )
			strcat (buf, "|");

		strcat (buf, params[cnt]);
	}

	addlog ("Event \"%s\", origin: \"%s\", params: %d [%s]", event, origin ? origin : "NULL", cnt, buf);
}

// Verbindung zum Chat Raum
void event_join (irc_session_t * session, const char * event, const char * origin, const char ** params, unsigned int count)
{
	dump_event (session, event, origin, params, count);
	irc_cmd_user_mode (session, "+i");
	irc_cmd_msg (session, params[0], "Hallo allerseits");
}

// Verbindung zum IRC Server
void event_connect (irc_session_t * session, const char * event, const char * origin, const char ** params, unsigned int count)
{
	irc_ctx_t * ctx = (irc_ctx_t *) irc_get_ctx (session);
	dump_event (session, event, origin, params, count);

	irc_cmd_join (session, ctx->channel, 0);
}


// Dieser event wird ausgelöst wenn man dem bot privat im chat eine nachricht schreibt
// er nimmt hier keine befehle entgegen sondern gibt nur das wieder was ihm geschrieben wurde
void event_privmsg (irc_session_t * session, const char * event, const char * origin, const char ** params, unsigned int count)
{
	dump_event (session, event, origin, params, count);

	printf ("'%s' sagte mir: (%s): %s\n", 
		origin ? origin : "jemand",
		params[0], params[1] );
}

// Die DCC Callbacks für die Datei Sendung oder Datei Empfang
void dcc_recv_callback (irc_session_t * session, irc_dcc_t id, int status, void * ctx, const char * data, unsigned int length)
{
	static int count = 1;
	char buf[12];

	switch (status)
	{
	case LIBIRC_ERR_CLOSED:
		printf ("DCC %d: chat geschlossen\n", id);
		break;

	case 0:
		if ( !data )
		{
			printf ("DCC %d: chat verbunden\n", id);
			irc_dcc_msg	(session, id, "Hehe");
		}
		else 
		{
			printf ("DCC %d: %s\n", id, data);
			sprintf (buf, "DCC [%d]: %d", id, count++);
			irc_dcc_msg	(session, id, buf);
		}
		break;

	default:
		printf ("DCC %d: error %s\n", id, irc_strerror(status));
		break;
	}
}


// DCC Callback für die Datie Sendung
void dcc_file_recv_callback (irc_session_t * session, irc_dcc_t id, int status, void * ctx, const char * data, unsigned int length)
{
	if ( status == 0 && length == 0 )
	{
		printf ("Datei senden Erfolgreich\n");

		if ( ctx )
			fclose ((FILE*) ctx);
	}
	else if ( status )
	{
		printf ("Datei senden Fehlgeschlagen Fehler: %d\n", status);

		if ( ctx )
			fclose ((FILE*) ctx);
	}
	else
	{
		if ( ctx )
			fwrite (data, 1, length, (FILE*) ctx);
		printf ("Fortschritt Datei Senden: %d\n", length);
	}
}


// Hier hört der bot auf bestimmte befehle die er dann ausführt
// Jede chat eingabe wird unabhängig davon ob es ein befehl ist oder nicht
// im terminal nochmal ausgegeben
void event_channel (irc_session_t * session, const char * event, const char * origin, const char ** params, unsigned int count)
{
	char nickbuf[128];

	if ( count != 2 )
		return;

	//
	printf ("'%s' sagte im channel %s: %s\n", 
		origin ? origin : "jemand",
		params[0], params[1] );

	if ( !origin )
		return;

	irc_target_get_nick (origin, nickbuf, sizeof(nickbuf));

	// dieser befehlt terminiert den bot
	if ( !strcmp (params[1], "quit") )
	{
		irc_cmd_quit (session, "Gewiss Meister!");
	}


	// gibt im chat die die möglichen befehle aus
	if ( !strcmp (params[1], "help") )
	{
		irc_cmd_msg (session, params[0], "quit, help, dcc chat, dcc send, ctcp, whois nickname");
	}

	// wird dem benutzer zurück geflüstert, kein plan wofür
	if ( !strcmp (params[1], "ctcp") )
	{
		irc_cmd_ctcp_request (session, nickbuf, "PING 223");
		irc_cmd_ctcp_request (session, nickbuf, "FINGER");
		irc_cmd_ctcp_request (session, nickbuf, "VERSION");
		irc_cmd_ctcp_request (session, nickbuf, "TIME");
	}

	// Siehe DCC link oben
	if ( !strcmp (params[1], "dcc chat") )
	{
		irc_dcc_t dccid;
		irc_dcc_chat (session, 0, nickbuf, dcc_recv_callback, &dccid);
		printf ("DCC chat ID: %d\n", dccid);
	}

	if ( !strcmp (params[1], "dcc send") )
	{
		irc_dcc_t dccid;
		irc_dcc_sendfile (session, 0, nickbuf, "irctest.c", dcc_file_recv_callback, &dccid);
		printf ("DCC send ID: %d\n", dccid);
	}

	// Befehlt den Bot den channel zu wegseln, zweiter Argument ist der channel name
	if ( strstr (params[1], "join") == params [1])
	{
		irc_cmd_join(session, (params[1] + 5), 0);
	}

	// gibt den topic vom channel aus
	if ( !strcmp (params[1], "topic") )
	{
		irc_cmd_topic (session, params[0], 0);
	}

	else if ( strstr (params[1], "topic ") == params[1] )
	{
		irc_cmd_topic (session, params[0], params[1] + 6);
	}

	// 
	if ( strstr (params[1], "mode ") == params[1] )
	{
		irc_cmd_channel_mode (session, params[0], params[1] + 5);
	}

	if ( strstr (params[1], "nick ") == params[1] )
	{
		irc_cmd_nick (session, params[1] + 5);
	}
	
	// gibt details über einen user im chat zurück, chat befehl dafür --> whois nickname
	if ( strstr (params[1], "whois ") == params[1] )
	{
		irc_cmd_whois (session, params[1] + 5);
	}
}

// Funktion wenn eine DCC Chat Verbindung angefordert wird
void irc_event_dcc_chat (irc_session_t * session, const char * nick, const char * addr, irc_dcc_t dccid)
{
	printf ("DCC chat [%d] angefordert von '%s' (%s)\n", dccid, nick, addr);

	irc_dcc_accept (session, dccid, 0, dcc_recv_callback);
}

// Funktion wenn über DCC eine Datei angefordert wird
void irc_event_dcc_send (irc_session_t * session, const char * nick, const char * addr, const char * filename, unsigned long size, irc_dcc_t dccid)
{
	FILE * fp;
	printf ("DCC send [%d] angefordert von '%s' (%s): %s (%lu bytes)\n", dccid, nick, addr, filename, size);

	if ( (fp = fopen ("file", "wb")) == 0 )
		abort();

	irc_dcc_accept (session, dccid, fp, dcc_file_recv_callback);
}

void event_numeric (irc_session_t * session, unsigned int event, const char * origin, const char ** params, unsigned int count)
{
	char buf[24];
	sprintf (buf, "%d", event);

	dump_event (session, buf, origin, params, count);
}
